# Commerce Europabank

## Features

The module provides off-site payment with the option to create a local payment method that maps directly to the payment method on the Europabank MPI (skipping off-site payment method selection).

### Install and configure the module

- Create a test account at [Europabank Online Testing](https://www.ebonline.be/test).
- Europabank will mail you some test cards and login information.
- Install the module using Composer and enable as usual.

```bash
$ composer require drupal/commerce_europabank
```

- Configure your Europabank ID and secret keys at /admin/commerce/config/payment.
- Add a new payment method and select the Europabank (Off-Site) plugin.
    - Optionally (but preferred), select the desired payment method.
    - In this case, you can add multiple payment plugins mapped to each off-site method, improving UX during checkout.

### Testing payments

- Ensure that your site is remotely accessible, as the payment status is only set to completed or authorized using a server-side notification.
    - Ngrok is a handy tool to achieve this during development. You can use ``ngrok`` to expose your local server to the internet. If you're using DDEV, ``ngrok`` is included, and you can share your site using the ``ddev share`` command.
    - Make sure to add the URL in the allowed URL section on the Europabank MPI admin pages to receive notifications properly.

### Developer notes

Europabank MPI integration manual can be found here: https://www.europabank.be/nl/download/ecommerce_manual
