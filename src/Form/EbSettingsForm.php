<?php

namespace Drupal\commerce_europabank\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Commerce Europabank settings for this site.
 */
final class EbSettingsForm extends ConfigFormBase {

  /**
   * List of supported modes.
   *
   * @var array
   */
  protected $modes = [
    'live' => 'Live',
    'test' => 'Test',
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'commerce_europabank_eb_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['commerce_europabank.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $eb_config = $this->config('commerce_europabank.settings');

    $form['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Mode'),
      '#options' => $this->modes,
      '#default_value' => $eb_config->get('mode') ?? 'test',
      '#required' => TRUE,
    ];

    $form['uid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MPI Account number'),
      '#description' => $this->t('The europabank MPI account number.'),
      '#required' => TRUE,
      '#default_value' => $eb_config->get('uid'),
    ];

    $form['server_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server secret'),
      '#description' => $this->t('The key used in communication between this site and europabank servers.'),
      '#required' => TRUE,
      '#default_value' => $eb_config->get('server_secret'),
    ];

    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secret'),
      '#description' => $this->t('The key used for communication between the clients browser and the the europabank servers.'),
      '#required' => TRUE,
      '#default_value' => $eb_config->get('client_secret'),
    ];

    $form['debug'] = [
      '#type' => 'checkbox',
      '#boolean' => TRUE,
      '#title' => $this->t('Enable debugging'),
      '#description' => $this->t('This enables watchdog logging of request/response and form data.'),
      '#default_value' => $eb_config->get('debug'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('commerce_europabank.settings')
      ->set('mode', $form_state->getValue('mode'))
      ->set('uid', $form_state->getValue('uid'))
      ->set('server_secret', $form_state->getValue('server_secret'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('debug', $form_state->getValue('debug'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
