<?php

namespace Drupal\commerce_europabank\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_europabank\EbRequest;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\InvalidResponseException;
use Drupal\commerce_payment\Exception\SoftDeclineException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsNotificationsInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "europabank_offsite",
 *   label = "Europabank (Off-site)",
 *   display_label = "Europabank",
 *   modes = {
 *     "n/a" = @Translation("N/A"),
 *   },
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_europabank\PluginForm\EbOffsiteForm",
 *   },
 *   payment_method_types = { },
 *   credit_card_types = { },
 *   requires_billing_information = TRUE,
 * )
 */
class EbOffsite extends OffsitePaymentGatewayBase implements SupportsNotificationsInterface {

  /**
   * List of supported EB payment methods.
   *
   * @var array
   */
  protected $paymentMethods = [
    'V' => 'Visa',
    'M' => 'MasterCard',
    'N' => 'Bancontact',
    'A' => 'Maestro',
    'I' => 'iDEAL',
    'C' => 'Credit',
    'D' => 'Debit',
  ];

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Europabank global config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $ebConfig;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setEbConfig($container->get('config.factory'));
    $instance->setLogger($container->get('logger.factory'));

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getMode() {
    return $this->ebConfig->get('mode');
  }

  /**
   * Sets global EB config.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal config factory.
   */
  public function setEbConfig(ConfigFactoryInterface $config_factory) {
    $this->ebConfig = $config_factory->get('commerce_europabank.settings');
  }

  /**
   * Get the EB global configuration.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The EB global config.
   */
  public function getEbConfig() {
    return $this->ebConfig;
  }

  /**
   * Sets the logger channel.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   Drupal logger channel factory.
   */
  public function setLogger(LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->logger = $logger_channel_factory->get('commerce_europabank');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'offsite_payment_method' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    if (!empty($this->ebConfig->get('uid'))) {
      $this->messenger()->addWarning($this->t('Europabank settings are missing. Please configure <a href="@settings_url">here</a>.', [
        '@settings_url' => Url::fromRoute('commerce_europabank.eb_settings')->toString(),
      ]));
    }

    $form['offsite_payment_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Offsite payment method'),
      '#empty_option' => $this->t('Select payment method'),
      '#description' => $this->t('Select the prefered payment method. When this is selected the user will start with the selected method on the offsite payment page.'),
      '#options' => $this->paymentMethods,
      '#default_value' => $this->configuration['offsite_payment_method'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['offsite_payment_method'] = $values['offsite_payment_method'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $this->log(__FUNCTION__, $request);

    $eb_request = new EbRequest($request);
    $this->processResponse($eb_request, $this->ebConfig->get('client_secret'));
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $this->log(__FUNCTION__, $request);

    try {
      $eb_request = new EbRequest($request);
      $payment = $this->processResponse($eb_request, $this->ebConfig->get('server_secret'));

      // Setting the payment state in this.
      if ($eb_request->isAuthorized()) {
        $state = $eb_request->isTypeSale() ? 'completed' : 'authorization';
        $payment
          ->setState($state)
          ->save();
      }
    }
    // We must set a 200 response code when SoftDeclineException is throwed,
    // otherwise MPI will retry for 10 times because an exception response
    // returns with HTTP code 500.
    catch (SoftDeclineException $e) {
      return new Response('', 200);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->log(__FUNCTION__, $request);

    return parent::onCancel($order, $request);
  }

  /**
   * Process EB response.
   *
   * @param \Drupal\commerce_europabank\EbRequest $ebRequest
   *   The request.
   * @param string $secret
   *   Indiciates if the the current request is called as
   *   notification or redirect.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The payment
   */
  public function processResponse(EbRequest $ebRequest, $secret) {
    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

    /** @var \Drupal\commerce_payment\Entity\Payment $payment */
    $payment = $payment_storage->load($ebRequest->getParam('PAYMENT_ID'));
    $payment
      ->setRemoteId($ebRequest->getParam('Id'))
      ->setRemoteState($ebRequest->getParam('Status'))
      ->save();

    if (!$ebRequest->isValid($secret)) {
      $payment
        ->setState('failed')
        ->save();

      throw new InvalidResponseException('The gateway response is not valid.');
    }

    if (!$ebRequest->isAuthorized()) {
      $payment
        ->setState('failed')
        ->save();

      throw new SoftDeclineException(sprintf('Payment has been declined by the gateway (STATUS: %s)', $ebRequest->getParam('Status')));
    }

    return $payment;
  }

  /**
   * Return the MPI redirect uri.
   *
   * @return string
   *   The redirect url used for communication.
   */
  public function getRedirectUri() {
    $base_url = 'https://www.ebonline.be/mpi/authenticate';

    if ($this->getMode() == 'test') {
      $base_url = 'https://www.ebonline.be/test/mpi/authenticate';
    }

    return $base_url;
  }

  /**
   * Logger helper function.
   *
   * @param string $function
   *   The name (prefix) that is used during logging.
   * @param \Symfony\Component\HttpFoundation\Request|array $data
   *   Given request or arbitrary data.
   */
  public function log($function, $data) {
    if (!$this->ebConfig->get('debug')) {
      return;
    }

    $method = 'n/a';
    if ($data instanceof Request) {
      $method = $data->getMethod();
      $data = EbRequest::getParams($data);
    }

    $this->logger->debug('Europabank: @method, @function: <pre>@data</pre>', [
      '@function' => $function,
      '@method' => $method,
      '@data' => var_export($data, TRUE),
    ]);
  }

}
