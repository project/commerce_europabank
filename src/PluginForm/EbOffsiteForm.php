<?php

namespace Drupal\commerce_europabank\PluginForm;

use Drupal\commerce_europabank\EbRequest;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Europabank offsite form.
 *
 * @package Drupal\commerce_europabank\PluginForm
 */
class EbOffsiteForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * ECommerceOffsiteForm constructor.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager services.
   */
  public function __construct(
    protected readonly TimeInterface $time,
    protected readonly LanguageManagerInterface $languageManager
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('datetime.time'),
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    // Save the payment entity, so we have an uniform way to update the payment.
    $payment->save();

    $order = $payment->getOrder();
    $billing_profile = $order->getBillingProfile();

    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $billing_address */
    $billing_address = $billing_profile->get('address')->first();

    /** @var \Drupal\commerce_europabank\Plugin\Commerce\PaymentGateway\EbOffsite $eb_plugin */
    $eb_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $eb_plugin->getConfiguration();

    // EB MPI does not have an option to set the cancelation url so we override
    // the return url to our own controller that checks the
    // response based on status (CA).
    $return_url = Url::fromRoute('commerce_europabank.checkout.return', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE]);

    $redirect_url = $eb_plugin->getRedirectUri();
    $redirect_method = 'post';

    $order_number = $order->getOrderNumber();
    if (!$order_number) {
      $order_number = $order->id();
    }

    $mandatory_data = [
      'Uid' => $eb_plugin->getEbConfig()->get('uid'),
      'Orderid' => $order->id() . '-' . $this->time->getCurrentTime(),
      'Amount' => (int) $eb_plugin->toMinorUnits($payment->getAmount()),
      'Description' => (string) $this->t('Order #@order', ['@order' => $order_number]),
    ];

    $data = [
      'Hash' => EbRequest::createHash($mandatory_data, $eb_plugin->getEbConfig()->get('client_secret')),
      'Beneficiary' => $order->getStore()->label(),
      'Chlanguage' => $this->languageManager->getCurrentLanguage()->getId(),
      'Chname' => $billing_address->getGivenName() . ' ' . $billing_address->getFamilyName(),
      'Chcountry' => $billing_address->getCountryCode(),
      'Chemail' => $order->getEmail(),
      'Redirecttype' => 'DIRECTGET',
      'Feedbacktype' => 'SEMIONLINE',
      'Redirecturl' => $return_url->toString(),
      'Feedbackurl' => $eb_plugin->getNotifyUrl()->toString(),
      'Merchantparam' => '&' . http_build_query([
        'PAYMENT_ID' => $payment->id(),
        'ORDER_ID' => $order->id(),
      ]),
    ];

    if (!empty($configuration['offsite_payment_method'])) {
      $data['Brand'] = $configuration['offsite_payment_method'];
    }

    $data = $mandatory_data + $data;
    $form = $this->buildRedirectForm($form, $form_state, $redirect_url, $data, $redirect_method);

    $eb_plugin->log(__FUNCTION__, $data + ['redirect_url' => $redirect_url]);

    return $form;
  }

}
