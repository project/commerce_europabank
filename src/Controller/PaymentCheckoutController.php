<?php

namespace Drupal\commerce_europabank\Controller;

use Drupal\commerce_europabank\EbRequest;
use Drupal\commerce_payment\Controller\PaymentCheckoutController as PaymentCheckoutControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Overrides Drupal\commerce_payment\Controller\PaymentCheckoutController.
 *
 * Uses the correct method based on availalbe query arguments
 * from the europabank response.
 *
 * Europabank does not have a cancel redirect path, so we need to
 * implement detection on this side.
 *
 * @package Drupal\commerce_europabank\Controller
 */
class PaymentCheckoutController extends PaymentCheckoutControllerBase {

  /**
   * {@inheritdoc}
   */
  public function returnPage(Request $request, RouteMatchInterface $route_match) {
    if ($request->query->get('Status') == EbRequest::STATUS_CANCELED) {
      return parent::cancelPage($request, $route_match);
    }

    return parent::returnPage($request, $route_match);
  }

}
