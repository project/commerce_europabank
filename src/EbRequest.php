<?php

namespace Drupal\commerce_europabank;

use Symfony\Component\HttpFoundation\Request;

/**
 * Helper functions for the commerce_europabank request.
 */
class EbRequest {

  /**
   * Cancelled by cardholder.
   */
  const STATUS_CANCELED = 'CA';

  /**
   * Authorized.
   */
  const STATUS_AUTHORIZED = 'AU';

  /**
   * Declined.
   */
  const STATUS_DECLINED = 'DE';

  /**
   * Technical problem occurred.
   */
  const STATUS_TECHNICAL_PROBLEM = 'EX';

  /**
   * Timed out.
   */
  const STATUS_TIMED_OUT = 'TI';

  /**
   * Tx type : Authorisation.
   */
  const TYPE_AUTHORIZATION = 02;

  /**
   * Tx type : Sale.
   */
  const TYPE_SALE = 05;

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  public function __construct(
    protected readonly Request $request) {
  }

  /**
   * Validates the current request 'Hash'.
   *
   * To verify if the request comes from the MPI.
   *
   * @param string $secret
   *   The secret to use.
   *
   * @return bool
   *   Validation result.
   */
  public function isValid($secret) {
    $params = [
      $this->getParam('Id'),
      $this->getParam('Orderid'),
    ];

    return $this->getParam('Hash') == self::createHash($params, $secret);
  }

  /**
   * Extended validation of the current request 'Ehash'.
   *
   * To verify if the request comes from the
   * MPI and has not been manipulated.
   *
   * @param string $secret
   *   The secret to use.
   *
   * @return bool
   *   Validation result.
   */
  public function isValidExtended($secret) {
    $params = [
      $this->getParam('Id'),
      $this->getParam('Orderid'),
      $this->getParam('Uid'),
      $this->getParam('Status'),
      $this->getParam('Brand'),
      $this->getParam('Refnr'),
      $this->getParam('Txtype'),
    ];

    return $this->getParam('Hash') == self::createHash($params, $secret);
  }

  /**
   * Returns if payment was successfull.
   *
   * @return bool
   *   Authorized or not.
   */
  public function isAuthorized() {
    return $this->getParam('Status') == self::STATUS_AUTHORIZED;
  }

  /**
   * Return if authorized payed = sale.
   *
   * @return bool
   *   Is sale or not
   */
  public function isTypeSale() {
    return $this->getParam('Txtype') == self::TYPE_SALE;
  }

  /**
   * Return if authorized payed = authorisation.
   *
   * @return bool
   *   Is sale or not
   */
  public function isTypeAuthorisation() {
    return $this->getParam('Txtype') == self::TYPE_AUTHORIZATION;
  }

  /**
   * Create sha1 hash of given data.
   *
   * @param array $data
   *   Array data to use.
   * @param string $secret
   *   The secret to use during hashing.
   *
   * @return string
   *   Hash of array data.
   */
  public static function createHash(array $data, string $secret): string {
    $combined = implode('', $data) . $secret;

    return strtoupper(sha1($combined));
  }

  /**
   * Return GET or POST params from request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return array
   *   Params
   */
  public static function getParams(Request $request): array {
    switch ($request->getMethod()) {
      case 'GET':
        return $request->query->all();

      case 'POST':
        return $request->request->all();
    }

    return [];
  }

  /**
   * Return a single param from the request params.
   *
   * @param string $key
   *   The param to return.
   *
   * @return mixed
   *   The param value.
   */
  public function getParam($key) {
    $params = self::getParams($this->request);

    return $params[$key] ?? NULL;
  }

}
